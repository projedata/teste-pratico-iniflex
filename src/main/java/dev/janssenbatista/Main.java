package dev.janssenbatista;

import dev.janssenbatista.models.Funcionario;
import dev.janssenbatista.models.Pessoa;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        var maria = new Funcionario("Maria", LocalDate.of(2000, 10, 18), BigDecimal.valueOf(2009.44), "Operador");
        var joao = new Funcionario("João", LocalDate.of(1990, 5, 12), BigDecimal.valueOf(2284.38), "Operador");
        var caio = new Funcionario("Caio", LocalDate.of(1961, 5, 2), BigDecimal.valueOf(9836.14), "Coordenador");
        var miguel = new Funcionario("Miguel", LocalDate.of(1988, 10, 14), BigDecimal.valueOf(19119.88), "Diretor");
        var alice = new Funcionario("Alice", LocalDate.of(1995, 1, 5), BigDecimal.valueOf(2234.68), "Recepcionista");
        var heitor = new Funcionario("Heitor", LocalDate.of(1999, 11, 19), BigDecimal.valueOf(1582.72), "Operador");
        var arthur = new Funcionario("Arthur", LocalDate.of(1993, 3, 31), BigDecimal.valueOf(4071.84), "Contador");
        var laura = new Funcionario("Laura", LocalDate.of(1994, 7, 8), BigDecimal.valueOf(3017.45), "Gerente");
        var heloisa = new Funcionario("Heloisa", LocalDate.of(2003, 5, 24), BigDecimal.valueOf(1606.85), "Eletricista");
        var helena = new Funcionario("Helena", LocalDate.of(1996, 9, 2), BigDecimal.valueOf(2799.93), "Gerente");

//        3.1 – Inserir todos os funcionários, na mesma ordem e informações da tabela acima.
        List<Funcionario> funcionarios =
                new ArrayList<>(List.of(maria, joao, caio, miguel, alice, heitor, arthur, laura, heloisa, helena));

//        3.2 – Remover o funcionário “João” da lista.
        funcionarios.remove(joao);

        /* 3.3 – Imprimir todos os funcionários com todas suas informações, sendo que:
            • informação de data deve ser exibido no formato dd/mm/aaaa;
            • informação de valor numérico deve ser exibida no formatado com separador de milhar como ponto e decimal como vírgula. */
        Funcionario.listarFuncionarios(funcionarios);


//        3.4 – Os funcionários receberam 10% de aumento de salário, atualizar a lista de funcionários com novo valor.
        funcionarios.forEach(f -> f.setSalario(f.getSalario().multiply(BigDecimal.valueOf(1.1))));
        System.out.println("Após aumento de 10%:");
        Funcionario.listarFuncionarios(funcionarios);

//        3.5 – Agrupar os funcionários por função em um MAP, sendo a chave a “função” e o valor a “lista de funcionários”.
        Map<String, List<Funcionario>> funcionariosPorFuncao = funcionarios.stream().collect(Collectors.groupingBy(Funcionario::getFuncao));

//        3.6 – Imprimir os funcionários, agrupados por função.
        System.out.println("Listando funcionários por função:");
        funcionariosPorFuncao.forEach((funcao, listaDeFuncionarios) -> {
            System.out.println("Função: " + funcao);
            listaDeFuncionarios.forEach(System.out::println);
        });
        System.out.println();

//        3.8 – Imprimir os funcionários que fazem aniversário no mês 10 e 12.
        System.out.println("Funcionários que fazem aniversário nos meses 10 ou 12");
        funcionarios.stream()
                .filter(f -> f.getDataNascimento().getMonth().getValue() == 10
                        || f.getDataNascimento().getMonth().getValue() == 12)
                .forEach(System.out::println);
        System.out.println();

//        3.9 – Imprimir o funcionário com a maior idade, exibir os atributos: nome e idade.
        System.out.println("Funcionário com a maior idade:");
        funcionarios.stream()
                .sorted(Comparator.comparing(Funcionario::getDataNascimento))
                .limit(1)
                .forEach(f -> System.out.println("Nome: " + f.getNome() + "\nIdade: " +
                        (Period.between(f.getDataNascimento(), LocalDate.now()).getYears()) + " anos"));
        System.out.println();

//        3.10 – Imprimir a lista de funcionários por ordem alfabética.
        System.out.println("Lista de funcionários por ordem alfabética:");
        List<Funcionario> funcionariosPorOrdemAlfabetica = funcionarios.stream().sorted(Comparator.comparing(Pessoa::getNome))
                .toList();
        Funcionario.listarFuncionarios(funcionariosPorOrdemAlfabetica);

//        3.11 – Imprimir o total dos salários dos funcionários.
        var salarioTotalDosFuncionarios = funcionarios.stream()
                .map(Funcionario::getSalario)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        System.out.println("---------------------------------------------------------------");
        System.out.println("Salário total dos funcionários: " + NumberFormat
                .getCurrencyInstance(new Locale("pt", "BR"))
                .format(salarioTotalDosFuncionarios));
        System.out.println("---------------------------------------------------------------");
        System.out.println();

//        3.12 – Imprimir quantos salários mínimos ganha cada funcionário, considerando que o salário mínimo é R$1212.00.
        System.out.println("Quantidade de salários mínimos que cada funcionário recebe:");
        funcionarios.forEach(f -> System.out.printf("O(A) funcionário(a) %s ganha %.3f salários mínimos\n",
                f.getNome(), f.getSalario().divide(BigDecimal.valueOf(1212), 3,RoundingMode.HALF_EVEN)));
    }
}