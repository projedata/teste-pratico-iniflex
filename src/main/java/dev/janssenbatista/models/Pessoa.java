package dev.janssenbatista.models;

import java.time.LocalDate;

public abstract class Pessoa {
    private final String nome;
    private final LocalDate dataNascimento;

    protected Pessoa(String nome, LocalDate dataNascimento) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

}
