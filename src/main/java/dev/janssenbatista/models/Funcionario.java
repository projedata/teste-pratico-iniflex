package dev.janssenbatista.models;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Funcionario extends Pessoa {

    private BigDecimal salario;
    private final String funcao;

    public Funcionario(String nome, LocalDate dataNascimento, BigDecimal salario, String funcao) {
        super(nome, dataNascimento);
        this.salario = salario;
        this.funcao = funcao;
    }

    public BigDecimal getSalario() {
        return salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }

    public String getFuncao() {
        return funcao;
    }

    @Override
    public String toString() {
        return "%-10s\t%-14s\t\t%-12s\t%-14s".formatted(
                getNome(),
                DateTimeFormatter.ofPattern("dd/MM/yyyy").format(getDataNascimento()),
                NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(getSalario()),
                getFuncao()
        );
    }

    public static void listarFuncionarios(List<Funcionario> funcionarios) {
        System.out.printf("%-10s\t%-14s\t\t%-12s\t%-14s\n", "Nome", "Data Nascimento", "Salário", "Função");
        System.out.println("---------------------------------------------------------------");
        funcionarios.forEach(System.out::println);
        System.out.println();
    }

}
